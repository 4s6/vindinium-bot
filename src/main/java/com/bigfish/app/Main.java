package com.bigfish.app;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.bigfish.config.RootApplicationConfig;

public class Main {

    public static void main(String[] args) throws Exception {
        new Main().init();
    }

    private void init() {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        applicationContext.register(RootApplicationConfig.class);
        applicationContext.refresh();
        applicationContext.registerShutdownHook();
        applicationContext.start();
    }
}
