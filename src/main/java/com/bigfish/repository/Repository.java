package com.bigfish.repository;

public interface Repository<T> {
    T get();

    void set(T value);

    default void clearAll() {
    }
}
