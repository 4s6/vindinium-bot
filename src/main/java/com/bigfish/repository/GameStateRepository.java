package com.bigfish.repository;

import java.util.concurrent.atomic.AtomicReference;

import com.bigfish.dto.GameState;

public class GameStateRepository implements Repository<GameState> {

    private final AtomicReference<GameState> gameStateAtomicReference = new AtomicReference();

    @Override
    public GameState get() {
        return gameStateAtomicReference.get();
    }

    @Override
    public void set(GameState value) {
        gameStateAtomicReference.set(value);
    }
}
