package com.bigfish.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.bigfish.game.GameLifeCycleManager;
import com.bigfish.game.ai.StrategyAdvisorImpl;
import com.bigfish.game.ai.behaviour.FindContestedMineBehaviour;
import com.bigfish.game.ai.behaviour.FindFreeMineBehaviour;
import com.bigfish.game.ai.behaviour.StayAliveBehaviour;
import com.bigfish.game.ai.rpa.OpponentStrongerRPA;
import com.bigfish.game.ai.rpa.RoutePerceptionAdjuster;
import com.bigfish.game.algo.Dijkstra;
import com.bigfish.game.algo.RouteFinder;
import com.bigfish.http.ApiHandler;
import com.bigfish.repository.GameStateRepository;

@Configuration
@ComponentScan({ "com.bigfish.game.ai.behaviour", "com.bigfish.game.algo" })
public class GameConfig {

    @Value("${game.loop}")
    private boolean loop;
    @Value("${game.key}")
    private String apiKey;
    @Value("${game.url}")
    private String url;
    @Value("${game.numberOfTurns}")
    private String numberOfTurns;
    @Value("${game.mapSize}")
    private String mapSize;

    @Value("${game.ai.lifeInDanger}")
    private Integer lifeInDanger;
    @Value("${game.ai.lifeWorthToRefillNextToBeer}")
    private Integer lifeWorthToRefillNextToBeer;

    @Bean
    public GameStateRepository gameStateRepository() {
        return new GameStateRepository();
    }

    @Bean(initMethod = "startGame")
    public GameLifeCycleManager simpleClient() {
        return new GameLifeCycleManager(loop, new ApiHandler(apiKey, url, numberOfTurns, mapSize), strategyAdvisor(),
                gameStateRepository(), routeFinder());
    }

    @Bean
    public StrategyAdvisorImpl strategyAdvisor() {
        return new StrategyAdvisorImpl(stayAliveBehaviour(), findMineBehaviour(), findContestedMineBehaviour(),
                routePerceptionAdjuster(), gameStateRepository());
    }

    @Bean
    public RoutePerceptionAdjuster routePerceptionAdjuster() {
        return new OpponentStrongerRPA(routeFinder(), gameStateRepository());
    }

    @Bean
    public StayAliveBehaviour stayAliveBehaviour() {
        return new StayAliveBehaviour(routeFinder(), gameStateRepository(), lifeInDanger, lifeWorthToRefillNextToBeer);
    }

    @Bean
    public RouteFinder routeFinder() {
        return new RouteFinder(dijkstra());
    }

    @Bean
    public Dijkstra dijkstra() {
        return new Dijkstra();
    }

    @Bean
    public FindFreeMineBehaviour findMineBehaviour() {
        return new FindFreeMineBehaviour(routeFinder(), gameStateRepository());
    }

    @Bean
    public FindContestedMineBehaviour findContestedMineBehaviour() {
        return new FindContestedMineBehaviour(routeFinder(), gameStateRepository());
    }
}
