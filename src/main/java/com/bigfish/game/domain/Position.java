package com.bigfish.game.domain;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class Position {

    private final int x;
    private final int y;
}
