package com.bigfish.game.domain;

import static com.google.api.client.util.Maps.newHashMap;
import static java.lang.Integer.MAX_VALUE;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import com.bigfish.game.algo.RouteFinder.Direction;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

@Getter
@EqualsAndHashCode(exclude = { "nodeType", "position", "neighbours", "distance", "prevNode" })
public class Node {

    private static final String DASH = "-";
    private final String nodeId;
    @Setter
    private NodeType nodeType;
    private final Position position;
    private final Map<Node, Long> neighbours = newHashMap();
    @Getter
    @Setter
    private Long distance = Long.valueOf(MAX_VALUE);
    @Getter
    @Setter
    private Node prevNode = null;

    @java.beans.ConstructorProperties({ "nodeId", "position" })
    public Node(String nodeId, NodeType nodeType, Position position) {
        this.nodeType = nodeType;
        this.nodeId = nodeId;
        this.position = position;
    }

    public Optional<Node> getNeighbour(Direction direction) {
        Optional<Node> neighbour;
        Stream<Node> stream = neighbours.keySet().stream();
        switch (direction) {
            case North:
                neighbour = stream.filter(node -> node.getPosition().getX() < this.getPosition().getX()).findFirst();
                break;
            case East:
                neighbour = stream.filter(node -> node.getPosition().getY() > this.getPosition().getY()).findFirst();
                break;
            case South:
                neighbour = stream.filter(node -> node.getPosition().getX() > this.getPosition().getX()).findFirst();
                break;
            case West:
                neighbour = stream.filter(node -> node.getPosition().getY() < this.getPosition().getY()).findFirst();
                break;
            default:
                neighbour = Optional.of(this);
                break;
        }
        return neighbour;

    }

    public void addNeighbour(Node node, Long weight) {
        neighbours.put(node, weight);
    }

    public void addNeighbour(Node node, Integer weight) {
        neighbours.put(node, Long.valueOf(weight));
    }

    private Node() {
        throw new NotImplementedException();
    }

    public boolean doesRouteGoThrough(NodeType nodeType) {
        boolean goesThrough = false;

        Node actualNode = this;
        while (actualNode != null) {
            goesThrough = actualNode.getNodeType().equals(nodeType);
            if (goesThrough && !actualNode.equals(this)) {
                break;
            }
            actualNode = actualNode.getPrevNode();
        }
        return goesThrough;
    }

    public void resetDistance() {
        distance = Long.valueOf(MAX_VALUE);
    }

    @Override
    public String toString() {
        StringBuilder routeBuilder = new StringBuilder();
        Node actualNode = this;
        while (actualNode != null) {
            routeBuilder.append(actualNode.getNodeType());
            routeBuilder.append(DASH);
            actualNode = actualNode.getPrevNode();
        }
        routeBuilder.append(distance);

        return routeBuilder.toString();
    }

    public boolean isNeighbour(NodeType type) {
        return neighbours.keySet().stream().anyMatch(node -> node.getNodeType().equals(type));
    }
}
