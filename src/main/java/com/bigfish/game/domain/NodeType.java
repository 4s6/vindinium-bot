package com.bigfish.game.domain;

import static java.lang.String.format;

import lombok.Getter;

public enum NodeType {

    GO("  "),
    NOGO("##"),
    MINE("$-"),
    MINE_P1("$1"),
    MINE_P2("$2"),
    MINE_P3("$3"),
    MINE_P4("$4"),
    BEER("[]"),
    PLAYER_1("@1"),
    PLAYER_2("@2"),
    PLAYER_3("@3"),
    PLAYER_4("@4");

    private static final String HERO_SIGNATURE_TEMPLATE = "@%s";
    private static final String MINE_SIGNATURE_TEMPLATE = "$%s";
    private static final String HERO_PREFIX = "@";
    @Getter
    private final String signature;

    NodeType(String signature) {
        this.signature = signature;
    }

    public boolean isHero(){
        return this.getSignature().startsWith(HERO_PREFIX);
    }

    public static NodeType getNodeType(String signature) {
        for (NodeType nodeType : values()) {
            if (nodeType.signature.equals(signature)) {
                return nodeType;
            }
        }
        throw new IllegalArgumentException(format("Can't recognise signature: %s", signature));
    }

    public static String getHeroSignature(int heroId) {
        return format(HERO_SIGNATURE_TEMPLATE, heroId);
    }

    public static String getMineSignature(int heroId) {
        return format(MINE_SIGNATURE_TEMPLATE, heroId);
    }
}
