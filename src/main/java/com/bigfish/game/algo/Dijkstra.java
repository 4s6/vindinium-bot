package com.bigfish.game.algo;

import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import com.bigfish.game.domain.Node;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public class Dijkstra {

    private static final Comparator<Node> SORT_BY_DISTANCE_COMPARATOR = (node1, node2) -> node1.getDistance()
            .compareTo(node2.getDistance());
    private static final String INITIAL_NODE_ERROR = "No initial node defined!";

    public List<Node> findRoute(List<Node> nodes) {
        final Queue<Node> remainingNodes = new LinkedBlockingQueue();
        nodes.sort(SORT_BY_DISTANCE_COMPARATOR);

        remainingNodes.addAll(nodes);

        if (remainingNodes.peek() != null && remainingNodes.peek().getDistance() != 0) {
            log.error(INITIAL_NODE_ERROR);
            throw new IllegalStateException(INITIAL_NODE_ERROR);
        }

        Node u;
        while ((u = remainingNodes.poll()) != null) {
            final Node actualNode = u;
            switch (actualNode.getNodeType()) {
                case GO:
                case PLAYER_1:
                case PLAYER_2:
                case PLAYER_3:
                case PLAYER_4:
                    actualNode.getNeighbours().entrySet().stream()
                            .forEach(neighbour -> updateRoutes(neighbour, actualNode, remainingNodes));
                default:
                    // No action taken, the rest of the nodes are not traversable
                    break;
            }

        }

        return nodes;
    }

    private void updateRoutes(Entry<Node, Long> entry, Node proposedNode, Queue<Node> remainingNodes) {
        Node node = entry.getKey();

        long proposedDistance = proposedNode.getDistance() + entry.getValue();

        if (proposedDistance < node.getDistance()) {
            node.setDistance(proposedDistance);
            node.setPrevNode(proposedNode);
            remainingNodes.add(node);
        }
    }
}
