package com.bigfish.game.algo;

import static com.bigfish.game.algo.RouteFinder.Direction.East;
import static com.bigfish.game.algo.RouteFinder.Direction.North;
import static com.bigfish.game.algo.RouteFinder.Direction.South;
import static com.bigfish.game.algo.RouteFinder.Direction.Stay;
import static com.bigfish.game.algo.RouteFinder.Direction.West;
import static com.google.common.collect.Lists.newArrayList;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.bigfish.game.domain.Node;
import com.bigfish.game.domain.NodeType;
import com.bigfish.game.domain.Position;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RequiredArgsConstructor
public class RouteFinder {

    private static final String NOT_NOT_FOUND = "Could not find node: {}";
    private static final long MIN_DISTANCE = 0L;

    private static final Comparator<Node> SORT_BY_DISTANCE_COMPARATOR = (node1, node2) -> node1.getDistance()
            .compareTo(node2.getDistance());
    private static final String GOING_FROM_TO_NODE = "Going from %s to %s";

    private final Dijkstra dijkstra;

    public enum Direction {
        Stay,
        North,
        South,
        East,
        West;
    }

    public List<Node> towardTypes(List<Node> nodes, NodeType fromType, NodeType toType) {
        Optional<Node> fromNode = nodes.stream().filter(node -> fromType.equals(node.getNodeType())).findFirst();
        List<Node> targetNodes = nodes.stream().filter(node -> toType.equals(node.getNodeType())).collect(toList());

        if (fromNode.isPresent() && !targetNodes.isEmpty()) {
            fromNode.get().setDistance(MIN_DISTANCE);
            dijkstra.findRoute(nodes);
        }
        else {
            targetNodes = newArrayList();
        }
        targetNodes.sort(SORT_BY_DISTANCE_COMPARATOR);
        return targetNodes;
    }

    public Direction toward(List<Node> nodes, NodeType fromType, NodeType toType) {

        Optional<Node> fromNode = nodes.stream().filter(node -> fromType.equals(node.getNodeType())).findFirst();
        Direction direction = Stay;
        Optional<Node> toNode = nodes.stream().filter(node -> toType.equals(node.getNodeType())).findFirst();
        if (fromNode.isPresent() && toNode.isPresent()) {
            return toward(nodes, fromNode.get(), toNode.get());
        } else {
            log.error(NOT_NOT_FOUND, toType);
        }
        return direction;
    }

    public Direction toward(List<Node> nodes, NodeType fromType, Node toNode) {
        Direction direction = Stay;
        Optional<Node> fromNode = nodes.stream().filter(node -> fromType.equals(node.getNodeType())).findFirst();
        if (fromNode.isPresent()) {
            direction = toward(nodes, fromNode.get(), toNode);
        }
        return direction;
    }

    public Direction toward(List<Node> nodes, Node fromNode, Node toNode) {
        Direction direction = Stay;
        if (!fromNode.equals(toNode)) {
            fromNode.setDistance(MIN_DISTANCE);
            dijkstra.findRoute(nodes);
            direction = getDirection(fromNode, toNode);
        }
        return direction;
    }

    private Direction getDirection(Node fromNode, Node toNode) {
        log.debug(format(GOING_FROM_TO_NODE, fromNode.getNodeType(), toNode.getNodeType()));
        Direction direction = Stay;
        Node actualNode = toNode;
        while (actualNode.getPrevNode() != null && !actualNode.getPrevNode().equals(fromNode)) {
            actualNode = actualNode.getPrevNode();
        }

        if (actualNode != null) {
            direction = determineStep(fromNode, actualNode);
        } else {
            log.error("Can't determine shortest path, surrounded?");
        }
        return direction;
    }

    private Direction determineStep(Node start, Node end) {
        Direction direction = Stay;
        Position startPosition = start.getPosition();
        Position endPosition = end.getPosition();

        if (startPosition.getX() == endPosition.getX() && startPosition.getY() < endPosition.getY()) {
            direction = East;
        } else if (startPosition.getX() == endPosition.getX() && startPosition.getY() > endPosition.getY()) {
            direction = West;
        } else if (startPosition.getY() == endPosition.getY() && startPosition.getX() < endPosition.getX()) {
            direction = South;
        } else if (startPosition.getY() == endPosition.getY() && startPosition.getX() > endPosition.getX()) {
            direction = North;
        } else if (startPosition.getY() == endPosition.getY() && startPosition.getX() == endPosition.getX()) {
            direction = Stay;
        }
        return direction;
    }
}
