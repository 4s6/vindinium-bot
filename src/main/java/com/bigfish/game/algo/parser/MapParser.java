package com.bigfish.game.algo.parser;

import static com.bigfish.game.domain.NodeType.getNodeType;
import static com.google.api.client.util.Lists.newArrayList;
import static java.lang.Math.sqrt;
import static java.lang.String.format;
import static java.lang.System.lineSeparator;

import java.util.List;

import com.bigfish.game.domain.Node;
import com.bigfish.game.domain.NodeType;
import com.bigfish.game.domain.Position;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MapParser {

    private static final String MAP_IS_INVALID = "Map is invalid";
    private static final int HALF_THE_VALUE = 2;
    private static final String DASH = "-";
    private static final int SIZE_OF_EACH_SIGNATURE = 2;
    private static final int ONE_POSITION = 1;
    private static final int WEIGHT = ONE_POSITION;
    private static final int FIRST_COLUMN = 0;
    private static final int FIRST_ROW = 0;
    private static final String MAP_SIZE_ERROR = "Map size calculation failed %f";

    public List<Node> parseMap(String map) {
        int numberOfNodesPerLine = verifyMapSize(map);
        log.debug("Map size {}", numberOfNodesPerLine);
        StringBuilder nodeInfo = new StringBuilder();
        nodeInfo.append(lineSeparator());
        List<Node> nodes = newArrayList();
        int nodeId = 0;
        for (int x = 0; x < numberOfNodesPerLine; x++) {
            for (int y = 0; y < numberOfNodesPerLine; y++) {
                int start = x * 2 * numberOfNodesPerLine + y * SIZE_OF_EACH_SIGNATURE;
                String signature = map.substring(start, start + SIZE_OF_EACH_SIGNATURE);
                NodeType nodeType = getNodeType(signature);
                Node node = new Node(String.valueOf(nodeId++), nodeType, new Position(x, y));
                nodes.add(node);
                nodeInfo.append(node.getNodeType()).append(DASH);
            }
            nodeInfo.append(lineSeparator());
        }
        log.debug(nodeInfo.toString());
        setNeighbours(nodes, numberOfNodesPerLine);
        return nodes;
    }

    private int verifyMapSize(String map) {
        if (map == null || map.length() % 2 != 0) {
            log.error(MAP_IS_INVALID);
            throw new IllegalArgumentException(MAP_IS_INVALID);
        }
        double mapSize = sqrt(map.length() / HALF_THE_VALUE);
        if (mapSize - (int) mapSize != 0) {
            String msg = format(MAP_SIZE_ERROR, mapSize);
            log.error(msg);
            throw new IllegalArgumentException(msg);
        }
        return (int) mapSize;
    }

    private void setNeighbours(List<Node> nodes, int size) {
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                int index = x * size + y;
                Node node = nodes.get(index);
                if (y != FIRST_COLUMN) { // LEFT Neighbour
                    node.addNeighbour(nodes.get(index - ONE_POSITION), WEIGHT);
                }
                if (y != size - ONE_POSITION) { // RIGHT Neighbour
                    node.addNeighbour(nodes.get(index + ONE_POSITION), WEIGHT);
                }
                if (x != FIRST_ROW) { // TOP Neighbour
                    node.addNeighbour(nodes.get((x - ONE_POSITION) * size + y), WEIGHT);
                }
                if (x != size - ONE_POSITION) { // BOTTOM Neighbour
                    node.addNeighbour(nodes.get((x + ONE_POSITION) * size + y), WEIGHT);
                }
            }
        }
    }
}
