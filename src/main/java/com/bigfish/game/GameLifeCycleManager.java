package com.bigfish.game;

import static com.bigfish.game.domain.NodeType.getHeroSignature;
import static com.bigfish.game.domain.NodeType.getNodeType;

import java.util.List;

import com.bigfish.dto.GameState;
import com.bigfish.dto.GameState.Board;
import com.bigfish.game.ai.StrategyAdvisorImpl;
import com.bigfish.game.algo.RouteFinder;
import com.bigfish.game.algo.RouteFinder.Direction;
import com.bigfish.game.algo.parser.MapParser;
import com.bigfish.game.domain.Node;
import com.bigfish.game.domain.NodeType;
import com.bigfish.http.ApiHandler;
import com.bigfish.repository.GameStateRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public class GameLifeCycleManager {

    private static final String GAME_URL = "Game URL: {}";
    private static final String ERROR_DURING_GAME_PLAY = "Error during game play";
    private static final String GAME_OVER = "Game over";
    private static final String EXCEPTION_WHILE_RUNNING_THE_GAME = "exception while running the game";
    private static final String TAKING_TURN = "taking turn: {} as {}, hero life {}";
    private static final String STEPPING = "Stepping {}";
    private final boolean loop;
    private final ApiHandler apiHandler;
    private final StrategyAdvisorImpl strategyAdvisorImpl;
    private final GameStateRepository gameStateRepository;
    private final RouteFinder routeFinder;

    public GameState startGame() throws Exception {
        GameState gameState = null;
        do {
            try {
                gameState = playGame(gameState);
            } catch (Throwable throwable) {
                log.error(EXCEPTION_WHILE_RUNNING_THE_GAME, throwable);
            }
        } while (loop);
        return gameState;
    }

    private GameState playGame(GameState gameState) {
        try {
            gameState = apiHandler.initGame();
            gameStateRepository.set(gameState);
            log.info(GAME_URL, gameState.getViewUrl());
            printBoard(gameState.getGame().getBoard());

            while (!gameState.getGame().isFinished() && !gameState.getHero().isCrashed()) {
                log.info(TAKING_TURN, gameState.getGame().getTurn(), getHeroSignature(gameState.getHero().getId()),
                        gameState.getHero().getLife());
                List<Node> nodes = new MapParser().parseMap(gameState.getGame().getBoard().getTiles());
                Node toThisNode = strategyAdvisorImpl.advice(nodes);
                NodeType yourHero = getNodeType(getHeroSignature(gameStateRepository.get().getHero().getId()));
                Direction toward = routeFinder.toward(nodes, yourHero, toThisNode);
                log.info(STEPPING, toward);

                gameState = apiHandler.move(toward.toString(), gameState.getPlayUrl());
                gameStateRepository.set(gameState);
                Board board = gameState.getGame().getBoard();
                printBoard(board);
            }

        } catch (Exception e) {
            log.error(ERROR_DURING_GAME_PLAY, e);
        }

        log.info(GAME_OVER);
        return gameState;
    }

    private void printBoard(Board board) {
        log.debug(board.getTiles());
        StringBuilder builder = new StringBuilder();
        builder.append(System.lineSeparator());
        for (int i = 0; i < board.getSize(); i++) {
            int start = i * board.getSize() * 2;
            String mapLine = board.getTiles().substring(start, start + board.getSize() * 2);
            builder.append(mapLine);
            builder.append(System.lineSeparator());
        }
        log.info(builder.toString());
    }
}
