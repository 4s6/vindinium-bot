package com.bigfish.game.ai.behaviour;

import static com.bigfish.game.domain.NodeType.MINE;
import static com.bigfish.game.domain.NodeType.getHeroSignature;
import static com.bigfish.game.domain.NodeType.getNodeType;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.bigfish.game.algo.RouteFinder;
import com.bigfish.game.domain.Node;
import com.bigfish.game.domain.NodeType;
import com.bigfish.repository.GameStateRepository;

@Component
public class FindFreeMineBehaviour extends MineBehaviour {

    private static final Node NOT_FOUND = null;
    private final RouteFinder routeFinder;

    @Inject
    public FindFreeMineBehaviour(RouteFinder routeFinder, GameStateRepository gameStateRepository) {
        super(gameStateRepository);
        this.routeFinder = routeFinder;
    }

    @Override
    public Node getTargetNode(List<Node> nodes) {
        NodeType heroType = getNodeType(getHeroSignature(gameStateRepository.get().getHero().getId()));
        List<Node> mines = routeFinder.towardTypes(nodes, heroType, MINE);
        Node selectedMine = mines.stream().findFirst().orElse(NOT_FOUND);
        return selectedMine != NOT_FOUND && checkIfMineSafeToTake(nodes, selectedMine) ? selectedMine : NOT_FOUND;
    }
}
