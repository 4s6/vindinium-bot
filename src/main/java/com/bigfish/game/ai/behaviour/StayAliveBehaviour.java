package com.bigfish.game.ai.behaviour;

import static com.bigfish.game.domain.NodeType.BEER;
import static com.bigfish.game.domain.NodeType.PLAYER_1;
import static com.bigfish.game.domain.NodeType.PLAYER_2;
import static com.bigfish.game.domain.NodeType.PLAYER_3;
import static com.bigfish.game.domain.NodeType.PLAYER_4;
import static com.bigfish.game.domain.NodeType.getHeroSignature;
import static com.bigfish.game.domain.NodeType.getNodeType;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.bigfish.dto.GameState.Hero;
import com.bigfish.game.algo.RouteFinder;
import com.bigfish.game.domain.Node;
import com.bigfish.game.domain.NodeType;
import com.bigfish.repository.GameStateRepository;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class StayAliveBehaviour implements Behaviour {

    private static final Node NO_PREFERENCE = null;
    private static final String PLAYERS_IN_ROUTE = "other player in route, trying to get another beer!";
    private static final int FIRST_BEER = 0;
    private final RouteFinder routeFinder;

    private final Integer lifeInDanger;
    private final Integer worthToRefill;

    public final GameStateRepository gameStateRepository;

    @Inject
    public StayAliveBehaviour(RouteFinder routeFinder, GameStateRepository gameStateRepository, Integer lifeInDanger,
            Integer worthToRefill) {
        this.routeFinder = routeFinder;
        this.gameStateRepository = gameStateRepository;
        this.lifeInDanger = lifeInDanger;
        this.worthToRefill = worthToRefill;
    }

    @Override
    public Node getTargetNode(List<Node> nodes) {
        return getSuggestedBeer(nodes, false);
    }

    private Node getSuggestedBeer(List<Node> nodes, boolean mustRefill) {
        Node suggestedBeer = NO_PREFERENCE;
        Hero hero = gameStateRepository.get().getHero();
        NodeType yourHero = getNodeType(getHeroSignature(hero.getId()));
        Node yourHeroNode = nodes.stream().filter(node -> node.getNodeType().equals(yourHero)).findFirst().get();

        if (mustRefill || hero.getLife() < lifeInDanger || yourHeroNode.isNeighbour(BEER)
                && hero.getLife() < worthToRefill) {
            NodeType heroType = getNodeType(getHeroSignature(hero.getId()));
            List<Node> beers = routeFinder.towardTypes(nodes, heroType, BEER);

            for (Node beer : beers) {
                suggestedBeer = beer;
                if (otherPlayersNotInRoute(heroType, suggestedBeer, nodes)) {
                    break;
                } else {
                    log.info(PLAYERS_IN_ROUTE);
                }
            }
            if (suggestedBeer == null) {
                Node firstBeer = beers.size() > 0 ? beers.get(FIRST_BEER) : NO_PREFERENCE;
                suggestedBeer = firstBeer != null ? firstBeer : NO_PREFERENCE;
            }
        }
        return suggestedBeer;
    }

    public Node getNearestBeer(List<Node> nodes) {
        return getSuggestedBeer(nodes, true);
    }

    private boolean otherPlayersNotInRoute(NodeType fromNodeType, Node target, List<Node> nodes) {
        Stream<NodeType> streamOfPlayers = Stream.<NodeType> builder().add(PLAYER_1).add(PLAYER_2).add(PLAYER_3)
                .add(PLAYER_4).build();
        List<NodeType> otherPlayers = streamOfPlayers.filter(nodeType -> !nodeType.equals(fromNodeType)).collect(
                toList());
        List<Node> conflictingPlayersInRoute = nodes.stream().filter(node -> otherPlayers.contains(node.getNodeType()))
                .filter(playerNode -> target.doesRouteGoThrough(playerNode.getNodeType()))
                .collect(toList());

        return conflictingPlayersInRoute.isEmpty();
    }
}
