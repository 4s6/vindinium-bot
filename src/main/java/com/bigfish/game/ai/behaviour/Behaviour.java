package com.bigfish.game.ai.behaviour;

import java.util.Comparator;
import java.util.List;

import com.bigfish.game.domain.Node;

public interface Behaviour {

    Comparator<Node> SORT_BY_DISTANCE_COMPARATOR = (node1, node2) -> node1.getDistance()
            .compareTo(node2.getDistance());

    Node getTargetNode(List<Node> nodes);
}
