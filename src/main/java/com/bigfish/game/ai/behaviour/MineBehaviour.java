package com.bigfish.game.ai.behaviour;

import static java.util.stream.Collectors.toList;

import java.util.List;

import com.bigfish.dto.GameState.Hero;
import com.bigfish.game.domain.Node;
import com.bigfish.game.domain.NodeType;
import com.bigfish.repository.GameStateRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
public abstract class MineBehaviour implements Behaviour {

    private static final int ONE_STEP = 1;
    private static final int THRESHOLD_OF_SAFE_DISTANCE_TO_TAKE_MINE = 3;
    private static final int THRESHOLD_TO_TAKE_MINE = 2;
    private static final int FIRST_ELEMENT = 0;
    private static final int ONE_HERO = 1;
    private static final int NO_HERO_INBOUND = 0;
    private static final int LIFE_LOST_IF_FIGHT_WITH_MINE = 20;
    private static final String LIFE_CHECK_MSG = "While considering taking the mine, life is {} minus 20 {} , opp life is {}";
    private static final String AMOUNT_OF_HEROES_NEAR = "amount of heroes near: {}";
    private static final int MIN_AND_OPP = 2;

    public final GameStateRepository gameStateRepository;

    protected boolean checkIfMineSafeToTake(List<Node> nodes, Node toNode) {
        boolean canTakeMine = true;
        Long distance = toNode.getDistance();
        Hero myHero = gameStateRepository.get().getHero();
        if (distance < THRESHOLD_TO_TAKE_MINE) {
            if (myHero.getLife() < LIFE_LOST_IF_FIGHT_WITH_MINE) {
                canTakeMine = false;
            } else {
                int myHeroId = myHero.getId();
                List<Node> heroes = nodes.stream().filter(node -> node.getNodeType().isHero()).collect(toList());
                Node myHeroNode = heroes
                        .stream()
                        .filter(heroNode -> heroNode.getNodeType().getSignature()
                                .equals(NodeType.getHeroSignature(myHeroId))).findFirst().get();
                heroes.remove(myHeroNode);
                List<Node> otherHeroInbound = heroes.stream()
                        .filter(otherHeroes -> otherHeroes.getDistance() <= THRESHOLD_OF_SAFE_DISTANCE_TO_TAKE_MINE)
                        .collect(toList());
                canTakeMine = otherHeroInbound.size() == ONE_HERO || otherHeroInbound.size() == NO_HERO_INBOUND;
                log.info(AMOUNT_OF_HEROES_NEAR, otherHeroInbound.size());
                if (otherHeroInbound.size() == ONE_HERO) {
                    Node node = otherHeroInbound.get(FIRST_ELEMENT);
                    String signature = node.getNodeType().getSignature();
                    Hero otherHero = gameStateRepository.get().getGame().getHeroes().stream()
                            .filter(otherHero2 -> signature.equals(NodeType.getHeroSignature(otherHero2.getId())))
                            .findFirst().get();
                    log.info(LIFE_CHECK_MSG, myHero.getLife(), myHero.getLife() - LIFE_LOST_IF_FIGHT_WITH_MINE,
                            otherHero.getLife());
                    canTakeMine = myHero.getLife() - MIN_AND_OPP * LIFE_LOST_IF_FIGHT_WITH_MINE > otherHero.getLife();
                }
            }
        }

        return canTakeMine;
    }
}
