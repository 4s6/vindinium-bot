package com.bigfish.game.ai.behaviour;

import static com.bigfish.game.domain.NodeType.MINE_P1;
import static com.bigfish.game.domain.NodeType.MINE_P2;
import static com.bigfish.game.domain.NodeType.MINE_P3;
import static com.bigfish.game.domain.NodeType.MINE_P4;
import static com.bigfish.game.domain.NodeType.getHeroSignature;
import static com.bigfish.game.domain.NodeType.getMineSignature;
import static com.bigfish.game.domain.NodeType.getNodeType;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.bigfish.game.algo.RouteFinder;
import com.bigfish.game.domain.Node;
import com.bigfish.game.domain.NodeType;
import com.bigfish.repository.GameStateRepository;

@Component
public class FindContestedMineBehaviour extends MineBehaviour {

    private static final Node NOT_FOUND = null;
    private final RouteFinder routeFinder;

    @Inject
    public FindContestedMineBehaviour(RouteFinder routeFinder, GameStateRepository gameStateRepository) {
        super(gameStateRepository);
        this.routeFinder = routeFinder;
    }

    @Override
    public Node getTargetNode(List<Node> nodes) {
        List<Node> mines = newArrayList();
        int heroId = gameStateRepository.get().getHero().getId();
        NodeType heroType = getNodeType(getHeroSignature(heroId));
        Stream<NodeType> mineTypesStream = Stream.<NodeType> builder().add(MINE_P1).add(MINE_P2).add(MINE_P3)
                .add(MINE_P4).build();
        mineTypesStream.filter(nodeType ->
                !nodeType.equals(getNodeType(getMineSignature(heroId)))).forEach(nodeType ->
                mines.addAll(routeFinder.towardTypes(nodes, heroType, nodeType)));

        mines.sort(SORT_BY_DISTANCE_COMPARATOR);
        Node selectedMine = mines.stream().findFirst().orElse(NOT_FOUND);
        return selectedMine != NOT_FOUND && checkIfMineSafeToTake(nodes, selectedMine) ? selectedMine : NOT_FOUND;
    }
}
