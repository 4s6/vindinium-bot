package com.bigfish.game.ai.rpa;

import static com.bigfish.game.domain.NodeType.NOGO;
import static com.bigfish.game.domain.NodeType.getHeroSignature;
import static com.bigfish.game.domain.NodeType.getNodeType;
import static java.util.stream.Collectors.toMap;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import com.bigfish.dto.GameState.Hero;
import com.bigfish.game.algo.RouteFinder;
import com.bigfish.game.domain.Node;
import com.bigfish.game.domain.NodeType;
import com.bigfish.repository.GameStateRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OpponentStrongerRPA implements RoutePerceptionAdjuster {

    private static final String HERO_PREFIX = "PLAYER";
    private static final int MUST_ACT_DISTANCE = 3;
    private static final int TEN_PERCENT = 10;
    private static final long OPPONENT_STRONGER_DISTANCE_ADJUSTMENT = (long) Integer.MAX_VALUE / TEN_PERCENT;
    private static final long OPPONENT_WEAKER_DISTANCE_ADJUSTMENT = 0;
    private static final int NO_MORE_ROUND = 0;
    private static final int STEP_IN_DISTANCE = 2;
    private static final Node NO_SUGGESTION = null;
    private static final int NO_ESCAPE = 1;
    private static final int WILL_DIE_IF_HIT = 20;
    private static final String STAYING = "staying where I am, I can beat him!";
    private static final String FIGH_ON = "Distance is {}, should I fight on?";
    private static final String KEEP_CHASING = "keep chasing!";
    private static final String OHTER_AND_MINE = "other hero life: {}, mylife {}";
    private static final String ADDING_WEIGHT = "adding more weight around this hero: {}";
    private static final String HERO_IS_WEAKER = "hero is weaker: {}";
    private static final String NOT_CHASING = "Not chasing!";
    private static final String ATTACK_WOULD_SUCCEED = "Attack would succeed if lifes close {}";
    private static final String FINDING_DISTANCE = "finding distance from: {} to {}";
    private final RouteFinder routeFinder;
    public final GameStateRepository gameStateRepository;

    @Inject
    public OpponentStrongerRPA(RouteFinder routeFinder, GameStateRepository gameStateRepository) {
        this.routeFinder = routeFinder;
        this.gameStateRepository = gameStateRepository;
    }

    @Override
    public Node modifyPerception(List<Node> nodes) {
        Hero yourHero = gameStateRepository.get().getHero();
        NodeType yourHeroType = getNodeType(getHeroSignature(yourHero.getId()));
        Map<NodeType, Node> heroes = nodes.stream().filter(node -> node.getNodeType().name().startsWith(HERO_PREFIX))
                .collect(toMap(
                        node -> node.getNodeType(),
                        node -> node)
                );
        Node yourHeroNode = heroes.remove(yourHeroType);

        return adjustDistanceBasedOnHeroesLife(nodes, yourHero, heroes, yourHeroNode);
    }

    private Node adjustDistanceBasedOnHeroesLife(List<Node> nodes, Hero yourHero, Map<NodeType, Node> heroesMap,
            Node yourHeroNode) {
        for (Node otherHeroNode : heroesMap.values()) {
            Hero otherHero = getHero(otherHeroNode);
            log.debug(FINDING_DISTANCE, yourHero.getName(), otherHeroNode.getNodeType());
            log.info(OHTER_AND_MINE, otherHero.getLife(), yourHero.getLife());
            routeFinder.toward(nodes, yourHeroNode, otherHeroNode);
            if (otherHeroNode.getDistance() == NO_ESCAPE) {
                if (otherHero.getLife() > yourHero.getLife() && otherHero.getLife() < WILL_DIE_IF_HIT
                        || otherHero.getLife() < yourHero.getLife()) {
                    log.info(STAYING);
                    return yourHeroNode;
                }
            }
            if (otherHeroNode.getDistance() == STEP_IN_DISTANCE) {
                log.info(FIGH_ON, otherHeroNode.getDistance());
                boolean attackWouldSucceed = otherHero.getLife() % WILL_DIE_IF_HIT + 1 < yourHero.getLife()
                        % WILL_DIE_IF_HIT;
                log.info(ATTACK_WOULD_SUCCEED, attackWouldSucceed);
                if (otherHero.getLife() < yourHero.getLife()
                        || otherHero.getLife() - yourHero.getLife() < WILL_DIE_IF_HIT && attackWouldSucceed) {
                    log.info(KEEP_CHASING);
                    return otherHeroNode.getPrevNode();
                }
                else {
                    log.info(NOT_CHASING);
                }
            }
            if (otherHeroNode.getDistance() <= MUST_ACT_DISTANCE) {
                if (otherHero.getLife() > yourHero.getLife()) {
                    log.info(ADDING_WEIGHT, getHeroSignature(otherHero.getId()));
                    adjustDistance(otherHeroNode, 2, OPPONENT_STRONGER_DISTANCE_ADJUSTMENT);

                }
                else {
                    log.info(HERO_IS_WEAKER, getHeroSignature(otherHero.getId()));
                    yourHeroNode.getNeighbours().put(otherHeroNode.getPrevNode(), OPPONENT_WEAKER_DISTANCE_ADJUSTMENT);
                }
            }

        }
        return NO_SUGGESTION;
    }

    private Hero getHero(Node otherHeroNode) {
        List<Hero> heroes = gameStateRepository.get().getGame().getHeroes();
        Hero otherHero = heroes
                .stream()
                .filter(hero -> otherHeroNode.getNodeType().getSignature()
                        .equals(getHeroSignature(hero.getId()))).findFirst()
                .get();
        log.debug("otherHero found: {}", otherHero.getName());
        return otherHero;
    }

    private void adjustDistance(Node hotNode, int layer, long weightAdjustment) {
        Set<Node> immediateNeighbours = hotNode.getNeighbours().keySet();
        immediateNeighbours
                .stream()
                .filter(notMeNode -> !notMeNode.getNodeType().isHero()
                ).forEach(node -> node.setNodeType(NOGO));
        immediateNeighbours.stream().forEach(
                immediateNeighbour -> immediateNeighbour.getNeighbours().put(hotNode,
                        weightAdjustment));
        if (--layer > NO_MORE_ROUND) {
            final int newLevel = layer;
            immediateNeighbours.forEach(nextHotNode -> adjustDistance(nextHotNode, newLevel,
                    weightAdjustment / TEN_PERCENT));
        }
    }
}
