package com.bigfish.game.ai;

import java.util.List;

import com.bigfish.game.ai.behaviour.FindContestedMineBehaviour;
import com.bigfish.game.ai.behaviour.FindFreeMineBehaviour;
import com.bigfish.game.ai.behaviour.StayAliveBehaviour;
import com.bigfish.game.ai.rpa.RoutePerceptionAdjuster;
import com.bigfish.game.domain.Node;
import com.bigfish.repository.GameStateRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
public class StrategyAdvisorImpl implements StrategyAdvisor {

    private final StayAliveBehaviour stayAliveBehaviour;

    private final FindFreeMineBehaviour findFreeMineBehaviour;

    private final FindContestedMineBehaviour findContestedMineBehaviour;

    private final RoutePerceptionAdjuster routePerceptionAdjuster;

    public final GameStateRepository gameStateRepository;

    @Override
    public Node advice(List<Node> nodes) {
        Node toNode;
        StringBuilder nodeSelectionProcess = new StringBuilder();
        nodeSelectionProcess.append("beginning route finding...");
        toNode = routePerceptionAdjuster.modifyPerception(nodes);

        nodes.stream().forEach(node -> node.resetDistance());
        if (toNode == null) {
            nodeSelectionProcess.append(",stayalive");
            toNode = stayAliveBehaviour.getTargetNode(nodes);
        }
        if (toNode == null) {
            nodeSelectionProcess.append(",checking for mine");
            Node freeMineNode = findFreeMineBehaviour.getTargetNode(nodes);
            Node occupiedMineNode = findContestedMineBehaviour.getTargetNode(nodes);
            if (freeMineNode != null && occupiedMineNode != null) {
                toNode = freeMineNode.getDistance() < occupiedMineNode.getDistance() ? freeMineNode : occupiedMineNode;
            }
            else if (freeMineNode == null && occupiedMineNode != null) {
                toNode = occupiedMineNode;
            }
            else if (freeMineNode != null && occupiedMineNode == null) {
                toNode = freeMineNode;
            }
        }
        if (toNode == null) {
            nodeSelectionProcess.append(",stayalive again");
            toNode = stayAliveBehaviour.getNearestBeer(nodes);
        }
        if (toNode == null) {
            nodeSelectionProcess.append(",not found anything!");
            log.error("Going Nowhere!");
        }
        nodeSelectionProcess.append("Decided for type: ");
        nodeSelectionProcess.append(toNode.getNodeType());
        log.info(nodeSelectionProcess.toString());

        return toNode;
    }
}
