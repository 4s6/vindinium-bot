package com.bigfish.game.ai;

import java.util.List;

import com.bigfish.game.domain.Node;

public interface StrategyAdvisor {

    Node advice(List<Node> nodes);
}
