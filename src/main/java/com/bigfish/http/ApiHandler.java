package com.bigfish.http;

import static com.google.api.client.util.Maps.newHashMap;

import java.io.IOException;
import java.util.Map;

import com.bigfish.dto.GameState;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.UrlEncodedContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.gson.GsonFactory;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public class ApiHandler {

    private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    private static final HttpRequestFactory REQUEST_FACTORY =
            HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {
                @Override
                public void initialize(HttpRequest request) {
                    request.setParser(new JsonObjectParser(JSON_FACTORY));
                }
            });

    private static final JsonFactory JSON_FACTORY = new GsonFactory();
    private static final String KEY = "key";
    private static final String DIR = "dir";
    private static final String MAP = "map";
    private static final String TURNS = "turns";
    private static final String INIT_API = "Sending initial request...";

    private final String apiKey;
    private final String gameUrl;
    private final String numberOfTurns;
    private final String mapSize;

    public GameState initGame() throws IOException {
        HttpContent content;
        HttpRequest request;
        log.info(INIT_API);

        Map<String, String> paramMap = newHashMap();
        paramMap.put(KEY, apiKey);
        paramMap.put(TURNS, numberOfTurns);
        paramMap.put(MAP, mapSize);

        content = new UrlEncodedContent(paramMap);

        request = REQUEST_FACTORY.buildPostRequest(new GenericUrl(gameUrl), content);

        request.getContent();
        request.setReadTimeout(0); // Wait forever to be assigned to a game

        return request.execute().parseAs(GameState.class);
    }

    public GameState move(String direction, String playUrl) throws IOException {
        Map<String, String> theMove = newHashMap();
        theMove.put(KEY, apiKey);
        theMove.put(DIR, direction);

        HttpContent turn = new UrlEncodedContent(theMove);
        HttpRequest turnRequest = REQUEST_FACTORY.buildPostRequest(new GenericUrl(playUrl), turn);
        HttpResponse turnResponse = turnRequest.execute();
        return turnResponse.parseAs(GameState.class);
    }
}
