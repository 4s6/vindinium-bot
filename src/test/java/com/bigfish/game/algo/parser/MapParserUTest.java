package com.bigfish.game.algo.parser;

import static com.bigfish.game.domain.NodeType.BEER;
import static com.bigfish.game.domain.NodeType.GO;
import static com.bigfish.game.domain.NodeType.MINE;
import static com.bigfish.game.domain.NodeType.NOGO;
import static com.bigfish.game.domain.NodeType.PLAYER_1;
import static com.bigfish.game.domain.NodeType.PLAYER_2;
import static com.bigfish.game.domain.NodeType.PLAYER_3;
import static com.bigfish.game.domain.NodeType.PLAYER_4;
import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static java.lang.Math.sqrt;
import static java.lang.String.format;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.IsInstanceOf.instanceOf;

import java.util.List;
import java.util.Optional;

import org.junit.Test;

import com.bigfish.game.domain.Node;
import com.bigfish.game.domain.NodeType;
import com.bigfish.game.domain.Position;

public class MapParserUTest {
    // @formatter:off
    private static final String invalidMap1 = "## ##";
    private static final String invalidMap2 = null; 
    private static final String MAP_0 = "  $-    " +
                                       "[]@1@2$-" +
                                       "  @3@4  " +
                                       "  ####  ";
    private static final int NUMBER_OF_NEIGHBOURS = 4;
    private static final String NODE_TYPE_NOT_FOUND = "can't find following node type: %s";
    private static final int HALF_IT = 2;
    private static final String MAP_1 = "##@1    ####    @4##      ########              ####            []        []    $-    ##    ##    $-$-    ##    ##    $-    []        []            ####  @3          ########      ##@2    ####      ##";
    private static final Position HERO_1_POSITION = new Position(0, 1);
    private static final Position HERO_2_POSITION = new Position(9, 1);
    private static final Position HERO_3_POSITION = new Position(7, 7);
    private static final Position HERO_4_POSITION = new Position(0, 8);
    /**
    ##@1    ####    @4##
          ########
            ####
        []        []
    $-    ##    ##    $-
    $-    ##    ##    $-
        []        []
            ####  @3
          ########
    ##@2    ####      ##
     */
    // @formatter:on
    private final MapParser mapParser = new MapParser();

    @Test
    public void validateHeroPositions() {
        List<Node> nodes = mapParser.parseMap(MAP_1);
        Node player1 = getNodeOfType(nodes, PLAYER_1);
        Node player2 = getNodeOfType(nodes, PLAYER_2);
        Node player3 = getNodeOfType(nodes, PLAYER_3);
        Node player4 = getNodeOfType(nodes, PLAYER_4);

        assertThat(player1.getPosition(), equalTo(HERO_1_POSITION));
        assertThat(player2.getPosition(), equalTo(HERO_2_POSITION));
        assertThat(player3.getPosition(), equalTo(HERO_3_POSITION));
        assertThat(player4.getPosition(), equalTo(HERO_4_POSITION));
    }

    @Test
    public void invalidMapRejected() {
        catchException(mapParser).parseMap(invalidMap1);
        assertThat(caughtException(), instanceOf(IllegalArgumentException.class));
    }

    @Test
    public void nullMapRejected() {
        catchException(mapParser).parseMap(invalidMap2);
        assertThat(caughtException(), instanceOf(IllegalArgumentException.class));
    }

    @Test
    public void invalidSizeMapRejected() {
        catchException(mapParser).parseMap(MAP_1.substring(0, 64));
        assertThat(caughtException(), instanceOf(IllegalArgumentException.class));
    }

    @Test
    public void opponentsAreIdentifiedCorrectly() {
        List<Node> nodes = mapParser.parseMap(MAP_0);
        Node player1 = getNodeOfType(nodes, PLAYER_1);
        Node player2 = getNodeOfType(nodes, PLAYER_2);
        Node player3 = getNodeOfType(nodes, PLAYER_3);
        Node player4 = getNodeOfType(nodes, PLAYER_4);

        validateNodeHas4Neighbours(player1, player2, player3, player4);

        assertThat(player1.getPosition(), equalTo(new Position(1, 1)));
        assertThat(player2.getPosition(), equalTo(new Position(1, 2)));
        assertThat(player3.getPosition(), equalTo(new Position(2, 1)));
        assertThat(player4.getPosition(), equalTo(new Position(2, 2)));

        validateIsNeighbour(player1, PLAYER_2, PLAYER_3, BEER, MINE);
        validateIsNeighbour(player2, PLAYER_1, PLAYER_4, MINE, GO);
        validateIsNeighbour(player3, PLAYER_1, PLAYER_4, NOGO, GO);
        validateIsNeighbour(player4, PLAYER_3, PLAYER_2, NOGO, GO);
    }

    @Test
    public void validatePositionIsSetCorrectly() {
        int numberOfNodesPerLine = ((int) sqrt(MAP_0.length() / HALF_IT));
        int y = 0;
        int x = 0;
        for (Node node : mapParser.parseMap(MAP_0)) {
            Position position = node.getPosition();
            assertThat(position, equalTo(new Position(x, y)));
            y++;
            if (y != 0 && y % numberOfNodesPerLine == 0) {
                y = 0;
                x++;
            }
        }
    }

    private void validateIsNeighbour(Node player1, NodeType... types) {
        for (int i = 0; i < types.length; i++) {
            NodeType type = types[i];
            Optional<Node> first = player1.getNeighbours().keySet().stream()
                    .filter(node -> node.getNodeType().equals(type)).findFirst();
            first.orElseThrow(() -> new IllegalStateException(format(NODE_TYPE_NOT_FOUND, type)));
        }
    }

    private void validateNodeHas4Neighbours(Node... nodes) {
        for (int i = 0; i < nodes.length; i++) {
            assertThat(nodes[i].getNeighbours().size(), equalTo(NUMBER_OF_NEIGHBOURS));
        }
    }

    private Node getNodeOfType(List<Node> nodes, NodeType desiredType) {
        return nodes.stream().filter(node -> node.getNodeType().equals(desiredType)).findFirst().get();
    }
}
