package com.bigfish.game.algo;

import static com.bigfish.game.domain.NodeType.GO;
import static com.google.api.client.util.Lists.newArrayList;
import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;

import java.util.List;
import java.util.Queue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.bigfish.game.domain.Node;
import com.bigfish.game.domain.Position;

@RunWith(MockitoJUnitRunner.class)
public class DijkstraUTest {

    private static final long MIN_DISTANCE = 0L;
    private static final long DISTANCE_TO_A = 0;
    private static final long DISTANCE_TO_B = 3;
    private static final long DISTANCE_TO_C = 2;
    private static final long DISTANCE_TO_D = 8;
    private static final long DISTANCE_TO_E = 10;
    private static final long DISTANCE_TO_Z = 13;
    private static final String NODE_ID_1 = "A";
    private static final String NODE_ID_2 = "B";
    private static final String NODE_ID_3 = "C";
    private static final String NODE_ID_4 = "D";
    private static final String NODE_ID_5 = "E";
    private static final String NODE_ID_6 = "Z";
    private static final Position POSITION_1 = new Position(1, 2);

    private final List<Node> nodes = newArrayList();

    private final Node nodeA = new Node(NODE_ID_1, GO, POSITION_1);
    private final Node nodeB = new Node(NODE_ID_2, GO, POSITION_1);
    private final Node nodeC = new Node(NODE_ID_3, GO, POSITION_1);
    private final Node nodeD = new Node(NODE_ID_4, GO, POSITION_1);
    private final Node nodeE = new Node(NODE_ID_5, GO, POSITION_1);
    private final Node nodeZ = new Node(NODE_ID_6, GO, POSITION_1);

    private Dijkstra dijkstra;

    @Mock
    private Queue<Node> mockRemainingNodes;

    @Before
    public void setup() {
        nodes.add(nodeB);
        nodes.add(nodeC);
        nodes.add(nodeD);
        nodes.add(nodeA);
        nodes.add(nodeE);
        nodes.add(nodeZ);

        nodeA.setDistance(MIN_DISTANCE);
        setNeighbours();
        dijkstra = new Dijkstra();
    }

    @Test
    public void shortestPathIsFoundToNodes() {
        dijkstra.findRoute(nodes);
        assertThat(nodeA.getDistance(), equalTo(DISTANCE_TO_A));
        assertThat(nodeB.getDistance(), equalTo(DISTANCE_TO_B));
        assertThat(nodeC.getDistance(), equalTo(DISTANCE_TO_C));
        assertThat(nodeD.getDistance(), equalTo(DISTANCE_TO_D));
        assertThat(nodeE.getDistance(), equalTo(DISTANCE_TO_E));
        assertThat(nodeZ.getDistance(), equalTo(DISTANCE_TO_Z));
    }

    @Test
    public void exceptionIsThrownIfNoInitialNodeDefined() {
        nodes.remove(nodeA);
        catchException(dijkstra).findRoute(nodes);
        assertThat(caughtException(), instanceOf(IllegalStateException.class));
    }

    private void setNeighbours() {

        nodeA.addNeighbour(nodeB, 4);
        nodeA.addNeighbour(nodeC, 2);

        nodeB.addNeighbour(nodeA, 4);
        nodeB.addNeighbour(nodeC, 1);
        nodeB.addNeighbour(nodeD, 5);

        nodeC.addNeighbour(nodeA, 2);
        nodeC.addNeighbour(nodeB, 1);
        nodeC.addNeighbour(nodeE, 10);
        nodeC.addNeighbour(nodeD, 8);

        nodeE.addNeighbour(nodeC, 10);
        nodeE.addNeighbour(nodeD, 2);
        nodeE.addNeighbour(nodeZ, 3);

        nodeD.addNeighbour(nodeB, 5);
        nodeD.addNeighbour(nodeC, 8);
        nodeD.addNeighbour(nodeE, 2);
        nodeD.addNeighbour(nodeZ, 6);

        nodeZ.addNeighbour(nodeD, 6);
        nodeZ.addNeighbour(nodeE, 3);
    }
}
