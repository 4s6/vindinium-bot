package com.bigfish.game.algo;

import static com.bigfish.game.algo.RouteFinder.Direction.East;
import static com.bigfish.game.algo.RouteFinder.Direction.North;
import static com.bigfish.game.algo.RouteFinder.Direction.South;
import static com.bigfish.game.algo.RouteFinder.Direction.Stay;
import static com.bigfish.game.algo.RouteFinder.Direction.West;
import static com.bigfish.game.domain.NodeType.BEER;
import static com.bigfish.game.domain.NodeType.PLAYER_1;
import static com.bigfish.game.domain.NodeType.PLAYER_2;
import static com.bigfish.game.domain.NodeType.PLAYER_3;
import static com.bigfish.game.domain.NodeType.PLAYER_4;
import static com.google.api.client.util.Lists.newArrayList;
import static java.lang.Integer.MAX_VALUE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;

import java.util.List;
import java.util.function.Predicate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.bigfish.game.algo.RouteFinder.Direction;
import com.bigfish.game.algo.parser.MapParser;
import com.bigfish.game.domain.Node;
import com.bigfish.game.domain.NodeType;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class RouteFinderUTest {

    private static final int SIZE_2 = 2;
    private static final int SIZE_3 = 3;
    private static final long DISTANCE_TWO = 2L;
    private static final long DISTANCE_FOUR = 4L;
    private static final long DISTANCE_SEVEN = 7L;
    private static final int FIRST_ELEMENT = 0;
    private static final int SECOND_ELEMENT = 1;
    private static final int THIRD_ELEMENT = 2;
    private Dijkstra dijkstra = new Dijkstra();
    private RouteFinder routeFinder = new RouteFinder(dijkstra);

    // @formatter:off
    private static final String MAP_0 ="  $-    " +
                                       "[]@1@2$-" +
                                       "  @3@4  " +
                                       "  ####  ";

    private static final String MAP_1 ="  @1  @2" +
                                       "  $-##  " +
                                       "  @3    " +
                                       "[]####@4";
    private static final String MAP_3 ="  @1    " +
                                       "[]$-##  " +
                                       "  @3  []" +
                                       "[]####@4";
    private static final String MAP_4 ="[]##@1  " +
                                       "##$-##  " +
                                       "        " +
                                       "[]####@4";
    // @formatter:on

    private static final List<Node> MAP_0_NODES = new MapParser().parseMap(MAP_0);

    private static final List<Node> MAP_3_NODES = new MapParser().parseMap(MAP_3);
    private static final List<Node> MAP_4_NODES = new MapParser().parseMap(MAP_4);

    @Mock
    private Node mockNode;

    @Test
    public void towardTypesReturnsNodesWithRightDistances() {
        List<Node> nodes = routeFinder.towardTypes(MAP_3_NODES, PLAYER_1, BEER);
        assertThat(nodes.size(), equalTo(SIZE_3));
        assertThat(nodes.get(FIRST_ELEMENT).getDistance(), equalTo(DISTANCE_TWO));
        assertThat(nodes.get(SECOND_ELEMENT).getDistance(), equalTo(DISTANCE_FOUR));
        assertThat(nodes.get(THIRD_ELEMENT).getDistance(), equalTo(Long.valueOf(MAX_VALUE)));
    }

    @Test
    public void towardTypesReturnsEmptyListIfFromNodeNotPresent() {
        List<Node> nodes = routeFinder.towardTypes(MAP_3_NODES, PLAYER_2, BEER);
        assertThat(nodes, empty());
    }

    @Test
    public void towardTypesReturnsNodesOrderedByDistance() {
        List<Node> nodes = routeFinder.towardTypes(MAP_4_NODES, PLAYER_1, BEER);
        assertThat(nodes.size(), equalTo(SIZE_2));
        assertThat(nodes.get(FIRST_ELEMENT).getDistance(), equalTo(DISTANCE_SEVEN));
        assertThat(nodes.get(SECOND_ELEMENT).getDistance(), equalTo(Long.valueOf(MAX_VALUE)));
    }

    @Test
    public void towardReturnsStayIfNodeNotFound() {
        Direction toward = routeFinder.toward(newArrayList(), PLAYER_1, BEER);
        assertThat(toward, equalTo(Stay));
    }

    @Test
    public void towardWithNodeInstanceReturnsStayIfNodeNotFound() {
        Direction toward = routeFinder.toward(newArrayList(), PLAYER_1, mockNode);
        assertThat(toward, equalTo(Stay));
    }

    @Test
    public void towardWithNodeInstanceProxiesRequest() {
        Node beer = MAP_0_NODES.stream().filter(node -> node.getNodeType().equals(BEER)).findFirst().get();
        Direction toward = routeFinder.toward(MAP_0_NODES, PLAYER_1, beer);
        assertThat(toward, equalTo(West));
    }

    @Test
    public void towardFindsNextStepToWestWhenDistanceIsOne() {
        Direction toward = routeFinder.toward(MAP_0_NODES, PLAYER_1, BEER);
        assertThat(toward, equalTo(West));
    }

    @Test
    public void towardFindsNextStepToEastWhenDistanceIsOne() {
        Direction toward = routeFinder.toward(MAP_0_NODES, PLAYER_1, PLAYER_2);
        assertThat(toward, equalTo(East));
    }

    @Test
    public void towardFindsNextStepToSouthWhenDistanceIsOne() {
        Direction toward = routeFinder.toward(MAP_0_NODES, PLAYER_1, PLAYER_3);
        assertThat(toward, equalTo(South));
    }

    @Test
    public void towardFindsNextStepToNorthWhenDistanceIsOne() {
        Direction toward = routeFinder.toward(MAP_0_NODES, PLAYER_3, PLAYER_1);
        assertThat(toward, equalTo(North));
    }

    @Test
    public void towardFindsNextStepToNorthWhenDistanceIsMoreThenOne() {
        Direction toward = routeFinder.toward(new MapParser().parseMap(MAP_1), PLAYER_2, PLAYER_4);
        assertThat(toward, equalTo(South));
    }

    @Test
    public void towardFindsRouteSouth() {
        List<Node> nodes = new MapParser().parseMap(MAP_1);
        assertThat(routeFinder.toward(nodes, PLAYER_2, PLAYER_4), equalTo(South));
    }

    @Test
    public void towardFindsRouteWest() {
        List<Node> nodes = new MapParser().parseMap(MAP_1);
        assertThat(routeFinder.toward(nodes, PLAYER_1, PLAYER_3), equalTo(West));
    }

    @Test
    public void towardFindsRouteEast() {
        List<Node> nodes = new MapParser().parseMap(MAP_1);
        assertThat(routeFinder.toward(nodes, PLAYER_1, PLAYER_2), equalTo(East));
    }

    @Test
    public void towardFindsRouteNorth() {
        List<Node> nodes = new MapParser().parseMap(MAP_1);
        assertThat(routeFinder.toward(nodes, PLAYER_4, PLAYER_3), equalTo(North));
    }

    @Test
    public void towardFindsFullRouteToNorth() {
        List<Direction> directions = getDirections(MAP_1, PLAYER_1, PLAYER_4);
        assertThat(directions, contains(East, East, South, South, South));
    }

    @Test
    public void towardFindsFullRouteToBeer() {
        List<Direction> directions = getDirections(MAP_1, PLAYER_4, BEER);
        assertThat(directions, contains(North, West, West, West, South));
    }

    private List<Direction> getDirections(String map1, NodeType fromNodeType, NodeType toNodeType) {

        List<Direction> directions = newArrayList();
        Direction direction;
        List<Node> nodes = new MapParser().parseMap(map1);

        Node fromNode = nodes.stream().filter(node -> fromNodeType.equals(node.getNodeType())).findFirst().get();
        Node toNode = nodes.stream().filter(node -> toNodeType.equals(node.getNodeType())).findFirst().get();

        while ((direction = routeFinder.toward(nodes, fromNode, toNode)) != Stay) {
            directions.add(direction);
            log.info(direction.name());
            Node actualNode = fromNode;
            fromNode = actualNode.getNeighbours().keySet().stream().filter(getNodePredicate(direction, actualNode))
                    .findFirst().get();
            nodes.stream().forEach(node -> node.setDistance((long) MAX_VALUE));
        }
        return directions;
    }

    private Predicate<Node> getNodePredicate(Direction direction, Node actualNode) {
        Predicate<Node> nodePredicate = null;
        switch (direction) {

            case North:
                nodePredicate = neighbour -> neighbour.getPosition().getX() < actualNode
                        .getPosition().getX();
                break;
            case South:
                nodePredicate = neighbour -> neighbour.getPosition().getX() > actualNode
                        .getPosition().getX();
                break;
            case West:
                nodePredicate = neighbour -> neighbour.getPosition().getY() < actualNode
                        .getPosition().getY();
                break;
            case East:
                nodePredicate = neighbour -> neighbour.getPosition().getY() > actualNode
                        .getPosition().getY();
                break;
        }
        return nodePredicate;
    }
}
