package com.bigfish.game.ai.behaviour;

import static com.bigfish.game.domain.NodeType.BEER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.bigfish.game.algo.Dijkstra;
import com.bigfish.game.algo.RouteFinder;
import com.bigfish.game.algo.parser.MapParser;
import com.bigfish.game.domain.Node;

@RunWith(MockitoJUnitRunner.class)
public class StayAliveBehaviourUTest extends BehaviourUTest {

    private Dijkstra dijkstra = new Dijkstra();

    private static final MapParser mapParser = new MapParser();
    private static final long DISTANCE_3 = 3L;
    private static final long DISTANCE_12 = 12L;
    private static final int HERO_LIFE_1 = 49;
    private static final int LIFE_IN_DANGER = 50;
    private static final int WORTH_TO_REFILL = 90;
    // @formatter:off
    private static final String MAP_1 = "####################################" +
                                        "################$1$1################" +
                                        "##########  []##    ##[]@4##########" +
                                        "##########  ##        ##  ##########" +
                                        "######$1    ####    ####@1  $-######" +
                                        "##########                ##########" +
                                        "########                    ########" +
                                        "######    ##  ########  ##    ######" +
                                        "######  $1##  ########  ##$-  ######" +
                                        "######  $-##  ########  ##$-  ######" +
                                        "######    ##  ########  ##    ######" +
                                        "########                    ########" +
                                        "##########                ##########" +
                                        "######$-    ####    ####    $-######" +
                                        "##########  ##        ##  ##########" +
                                        "##########@2[]##    ##[]@3##########" +
                                        "################$-$-################" +
                                        "####################################";
    // @formatter:on
    @Before
    public void setup() {
        super.setup();
        targetNodeType = BEER;
        behaviour = new StayAliveBehaviour(mockRouteFinder, mockGameStateRepository, LIFE_IN_DANGER, WORTH_TO_REFILL);
        when(mockNode2.getNodeType()).thenReturn(targetNodeType);
        when(mockGameStateRepository.get().getHero().getLife()).thenReturn(HERO_LIFE_1);
    }

    @Test
    public void algoFindsOtherPlayerInBottleNeck() {
        behaviour = new StayAliveBehaviour(new RouteFinder(dijkstra), mockGameStateRepository, LIFE_IN_DANGER, WORTH_TO_REFILL);
        Node targetNode = behaviour.getTargetNode(mapParser.parseMap(MAP_1));
        assertThat(targetNode.getDistance(), not(equalTo(DISTANCE_3)));
        assertThat(targetNode.getDistance(), equalTo(DISTANCE_12));
    }

    @Test
    public void getTargetNodeReturnNullIfDoesNotFindBeer() {
        nodes.add(mockNode1);
        nodes.add(mockNode2);
        Node targetNode = behaviour.getTargetNode(nodes);
        assertThat(targetNode, nullValue());
    }
}
