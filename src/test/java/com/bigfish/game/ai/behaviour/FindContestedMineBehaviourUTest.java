package com.bigfish.game.ai.behaviour;

import static com.bigfish.game.domain.NodeType.MINE_P1;
import static com.bigfish.game.domain.NodeType.MINE_P2;
import static com.bigfish.game.domain.NodeType.PLAYER_1;
import static com.bigfish.game.domain.NodeType.getMineSignature;
import static com.bigfish.game.domain.NodeType.getNodeType;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.bigfish.game.domain.Node;
import com.bigfish.game.domain.NodeType;

@RunWith(MockitoJUnitRunner.class)
public class FindContestedMineBehaviourUTest extends BehaviourUTest {

    private static final long DISTANCE_10 = 10L;
    private static final long DISTANCE_5 = 5L;
    @Mock
    protected Node mockMineNode1;
    @Mock
    protected Node mockMineNode2;
    @Mock
    protected Node mockMineNode3;
    @Mock
    protected Node mockMineNode4;

    @Before
    public void setup() {
        super.setup();
        targetNodeType = MINE_P2;
        behaviour = new FindContestedMineBehaviour(mockRouteFinder, mockGameStateRepository);
        when(mockNode1.getNodeType()).thenReturn(targetNodeType);
    }

    @Test
    public void adviceFindsClosestContestedMind() {
        List<Node> mineNodes = Stream.<Node> builder().add(mockMineNode1).add(mockMineNode2).add(mockMineNode3)
                .add(mockMineNode4).build().collect(toList());
        setDistances();
        List<Node> targetNodes = newArrayList();
        targetNodes.add(mockTargetNode);
        when(mockRouteFinder.towardTypes(nodes, PLAYER_1, targetNodeType)).thenReturn(mineNodes);
        Node targetNode = behaviour.getTargetNode(nodes);
        assertThat(targetNode, is(mockMineNode3));
    }

    private void setDistances() {
        when(mockMineNode1.getDistance()).thenReturn(DISTANCE_10);
        when(mockMineNode2.getDistance()).thenReturn(DISTANCE_10);
        when(mockMineNode2.getNodeType()).thenReturn(MINE_P1);
        when(mockMineNode3.getDistance()).thenReturn(DISTANCE_5);
        when(mockMineNode4.getDistance()).thenReturn(DISTANCE_10);
    }
}
