package com.bigfish.game.ai.behaviour;

import static com.bigfish.game.domain.NodeType.PLAYER_1;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.mockito.Mock;

import com.bigfish.dto.GameState.Hero;
import com.bigfish.game.algo.RouteFinder;
import com.bigfish.game.domain.Node;
import com.bigfish.game.domain.NodeType;
import com.bigfish.repository.GameStateRepository;

public abstract class BehaviourUTest {

    protected static final int HERO_ID = 1;
    private static final int FULL_LIFE = 100;
    protected static final Long DISTANCE_5 = 5l;
    @Mock
    protected Node mockNode1;

    @Mock
    protected Node mockNode2;
    @Mock
    protected Node mockTargetNode;
    @Mock(answer = RETURNS_DEEP_STUBS)
    protected GameStateRepository mockGameStateRepository;
    @Mock
    protected RouteFinder mockRouteFinder;

    @Mock
    private Hero mockHero;

    protected List<Node> nodes = newArrayList();

    protected Behaviour behaviour;

    protected NodeType targetNodeType;

    public void setup() {
        when(mockHero.getId()).thenReturn(HERO_ID);
        when(mockHero.getLife()).thenReturn(FULL_LIFE);
        when(mockGameStateRepository.get().getHero()).thenReturn(mockHero);
        when(mockNode1.getNodeType()).thenReturn(PLAYER_1);
        when(mockTargetNode.getNodeType()).thenReturn(targetNodeType);
        when(mockTargetNode.getDistance()).thenReturn(DISTANCE_5);
    }

    @Test
    public void getTargetNodeFindsFirstElementForGivenType() {
        nodes.add(mockNode1);
        List<Node> targetNodes = newArrayList();
        targetNodes.add(mockTargetNode);
        when(mockRouteFinder.towardTypes(nodes, PLAYER_1, targetNodeType)).thenReturn(targetNodes);
        Node targetNode = behaviour.getTargetNode(nodes);
        assertThat(targetNode, is(mockTargetNode));
    }
}
