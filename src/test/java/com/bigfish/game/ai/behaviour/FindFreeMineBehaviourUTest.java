package com.bigfish.game.ai.behaviour;

import static com.bigfish.game.domain.NodeType.MINE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.bigfish.game.domain.Node;

@RunWith(MockitoJUnitRunner.class)
public class FindFreeMineBehaviourUTest extends BehaviourUTest {

    @Before
    public void setup() {
        super.setup();
        targetNodeType = MINE;
        behaviour = new FindFreeMineBehaviour(mockRouteFinder, mockGameStateRepository);
        when(mockNode1.getNodeType()).thenReturn(targetNodeType);
    }

    @Test
    public void getTargetNodeReturnNullIfDoesNotFindBeer() {
        Node targetNode = behaviour.getTargetNode(nodes);
        assertThat(targetNode, nullValue());
    }
}
