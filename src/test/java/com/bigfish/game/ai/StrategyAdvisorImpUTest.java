package com.bigfish.game.ai;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.bigfish.game.ai.behaviour.FindContestedMineBehaviour;
import com.bigfish.game.ai.behaviour.FindFreeMineBehaviour;
import com.bigfish.game.ai.behaviour.StayAliveBehaviour;
import com.bigfish.game.ai.rpa.RoutePerceptionAdjuster;
import com.bigfish.game.domain.Node;
import com.bigfish.repository.GameStateRepository;

@RunWith(MockitoJUnitRunner.class)
public class StrategyAdvisorImpUTest {

    private static final Node NO_PREFERENCE = null;

    private List<Node> nodes = newArrayList();

    @Mock
    private StayAliveBehaviour mockStayAliveBehaviour;
    @Mock
    private FindFreeMineBehaviour mockFindFreeMineBehaviour;
    @Mock
    private FindContestedMineBehaviour mockFindContestedMineBehaviour;
    @Mock
    private RoutePerceptionAdjuster mockRoutePerceptionAdjuster;
    @Mock
    private Node mockStayNode;
    @Mock
    private Node mockMineNode;
    @Mock
    public GameStateRepository mockGameStateRepository;

    private StrategyAdvisorImpl strategyAdvisor;

    @Before
    public void setup() {
        strategyAdvisor = new StrategyAdvisorImpl(mockStayAliveBehaviour, mockFindFreeMineBehaviour,
                mockFindContestedMineBehaviour, mockRoutePerceptionAdjuster, mockGameStateRepository);
        when(mockStayAliveBehaviour.getTargetNode(any())).thenReturn(mockStayNode);
        when(mockFindFreeMineBehaviour.getTargetNode(any())).thenReturn(mockMineNode);
    }

    @Test
    public void stayAliveBehaviourCalledIfLifeInDanger() {
        Node advice = strategyAdvisor.advice(nodes);

        verify(mockStayAliveBehaviour).getTargetNode(nodes);
        assertThat(advice, equalTo(advice));
    }

    @Test
    public void findMineBehaviourCalledIfLifeIsNotInDanger() {
        when(mockStayAliveBehaviour.getTargetNode(any())).thenReturn(NO_PREFERENCE);
        Node advice = strategyAdvisor.advice(nodes);

        verify(mockFindFreeMineBehaviour).getTargetNode(nodes);
        assertThat(advice, equalTo(advice));
    }
}
