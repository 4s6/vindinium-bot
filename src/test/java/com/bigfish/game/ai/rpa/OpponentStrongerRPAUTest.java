package com.bigfish.game.ai.rpa;

import static com.bigfish.game.algo.RouteFinder.Direction.South;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.bigfish.dto.GameState.Hero;
import com.bigfish.game.algo.Dijkstra;
import com.bigfish.game.algo.RouteFinder;
import com.bigfish.game.algo.parser.MapParser;
import com.bigfish.game.domain.Node;
import com.bigfish.game.domain.NodeType;
import com.bigfish.repository.GameStateRepository;

@RunWith(MockitoJUnitRunner.class)
public class OpponentStrongerRPAUTest {

    private static final MapParser mapParser = new MapParser();
    private static final List<Hero> heros = newArrayList();
    private static final int HALF_IT = 2;

    private static final long OPPONENT_TOO_STRONG_DISTANCE_ADJUSTMENT = (long) Integer.MAX_VALUE / HALF_IT;
    private static final long OPPONENT_WEAKER_DISTANCE_ADJUSTMENT = (-1) * (long) Integer.MAX_VALUE / HALF_IT;
    private static final int ONE_LIFE = 1;
    private static final int TWO_LIFE = 2;
    private static final int HERO_ID_1 = 1;
    private static final int HERO_ID_2 = 2;
    private static final int HERO_ID_4 = 4;
    private static final int HERO_ID_3 = 3;
    private static final long DISTANCE_1 = 1;
    private static final int EXPECTED_SIZE = 5;
    private static final int NO_DISTANCE = 0;

    @Mock(answer = RETURNS_DEEP_STUBS)
    protected GameStateRepository mockGameStateRepository;

    @Mock
    private Hero mockYourHero;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private Hero mockOtherHero1;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private Hero mockOtherHero2;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private Hero mockOtherHero3;

    // I NEED A DESIGNATED List of NODES whereTO the distance is set back to half of the infinite!
    private OpponentStrongerRPA opponentStrongerRPA;

    private Dijkstra dijkstra;

    // @formatter:off
    private static final String MAP_1 = "      ####################################      " +
                                        "  ##    $2####    ############    ####$4    ##  " +
                                        "    ##  ##$2[]      ########      []$3##  ##    " +
                                        "        ##        ##  ####  ##      @3##        " +
                                        "  ##    ##    ####    ####    ####    ##    ##  " +
                                        "  ##$2  ##    ####    ####    ####    ##  $4##  " +
                                        "  ####      ######  ##    ##  ######@4    ####  " +
                                        "  ##  ##    ##$2##            ##$3##    ##  ##  " +
                                        "        ##  @1  ####  ####  ####      ##        " +
                                        "####    $2                            $4    ####" +
                                        "######    ##  ##$2            $3##  ##    ######" +
                                        "####    $-######                ######$4    ####" +
                                        "####    $-######                ######$-    ####" +
                                        "######    ##  ##$2            $3##  ##    ######" +
                                        "####    $2                            $3    ####" +
                                        "        ##      ####  ####  ####      ##        " +
                                        "  ##  ##    ##$2##            ##$3##    ##  ##  " +
                                        "  ####      ######  ##    ##  ######      ####  " +
                                        "  ##$2  ##@2  ####    ####    ####    ##  $3##  " +
                                        "  ##    ##    ####    ####    ####    ##    ##  " +
                                        "        ##        ##  ####  ##        ##        " +
                                        "    ##  ##$2[]      ########      []$3##  ##    " +
                                        "  ##    $2####    ############    ####$3    ##  " +
                                        "      ####################################      ";
    private static final String MAP_2 = "  @1  " +
                                        "      " +
                                        "  @2  ";

    private static final String MAP_3 = "######$-    ################################    $-######" +
                                        "######  ##  ####    $-############$-    ####  ##  ######" +
                                        "##              ##    $-########$-    ##              ##" +
                                        "##  ####                ########                ####  ##" +
                                        "##  ##    $-####      ############      ####$-    ##  ##" +
                                        "      ##$-##      ########    ########      ##$-##      " +
                                        "    ########        ##$-##    ##$-##        ########    " +
                                        "####  ######$2  ##  ##            ##  ##  $-######  ####" +
                                        "####  ####      ##                    ##      ####  ####" +
                                        "##        ##      ##                ##      ##        ##" +
                                        "    ####    ####  ##                ##  ####    ####    " +
                                        "$-    []              ##        ##              []    $-" +
                                        "##    ##  ##  $2$2$2                $3$3$3  ##  ##    ##" +
                                        "##$-######    ##        ##    ##        ##    ######$-##" +
                                        "##$2######    ##        ##    ##        ##    ######$-##" +
                                        "##    ##  ##  $2$2$2                $4$4$4  ##  ##    ##" +
                                        "$2    []@1            ##        ##              []    $-" +
                                        "    ####    ####  ##                ##  ####    ####    " +
                                        "##      @2##      ##                ##      ##        ##" +
                                        "####  ####      ##                    ##      ####  ####" +
                                        "####  ######$-  ##  ##            ##  ##  $3######  ####" +
                                        "    ########        ##$-##    ##$-##        ########    " +
                                        "      ##$-##      ########    ########      ##$-##      " +
                                        "##  ##    $-####      ############      ####$-    ##  ##" +
                                        "##  ####                ########                ####  ##" +
                                        "##              ##    $-########$-    ##              ##" +
                                        "######  ##  ####    $-############$-    ####  ##  ######" +
                                        "######$-    ################################    $-######";
    // @formatter:on

    @Before
    public void setup() {
        dijkstra = new Dijkstra();
        opponentStrongerRPA = new OpponentStrongerRPA(new RouteFinder(dijkstra), mockGameStateRepository);

    }

    @Test
    public void routePerceptionModifiedIfOpponentStronger() {
        List<Node> nodes = mapParser.parseMap(MAP_1);
        when(mockGameStateRepository.get().getGame().getHeroes()).thenReturn(heros);
        when(mockGameStateRepository.get().getHero()).thenReturn(mockYourHero);
        prepareHeroesForScenarioV1();

        heros.add(mockYourHero);
        heros.add(mockOtherHero1);

        opponentStrongerRPA.modifyPerception(nodes);
    }

    @Test
    public void routePerceptionModifiedIfOpponentIsWeakerAndClose() {
        List<Node> nodes = mapParser.parseMap(MAP_2);
        when(mockGameStateRepository.get().getGame().getHeroes()).thenReturn(heros);
        when(mockGameStateRepository.get().getHero()).thenReturn(mockYourHero);

        when(mockYourHero.getLife()).thenReturn(TWO_LIFE);
        when(mockYourHero.getId()).thenReturn(HERO_ID_1);
        when(mockOtherHero1.getId()).thenReturn(HERO_ID_2);
        when(mockOtherHero1.getLife()).thenReturn(ONE_LIFE);

        heros.add(mockYourHero);
        heros.add(mockOtherHero1);

        Node suggestedNode = opponentStrongerRPA.modifyPerception(nodes);
        Node myHeroNode = nodes.stream()
                .filter(node -> node.getNodeType().getSignature().equals(NodeType.getHeroSignature(HERO_ID_1)))
                .findFirst().get();
        assertThat(myHeroNode.getNeighbour(South).get(), is(suggestedNode));
    }

    @Test
    public void routePerceptionModifiedIfOpponentIsWeakerAndCloseV2() {
        List<Node> nodes = mapParser.parseMap(MAP_3);
        when(mockGameStateRepository.get().getGame().getHeroes()).thenReturn(heros);
        when(mockGameStateRepository.get().getHero()).thenReturn(mockYourHero);

        when(mockYourHero.getLife()).thenReturn(ONE_LIFE);
        when(mockYourHero.getId()).thenReturn(HERO_ID_2);
        when(mockOtherHero1.getId()).thenReturn(HERO_ID_1);
        when(mockOtherHero1.getLife()).thenReturn(TWO_LIFE);

        heros.add(mockYourHero);
        heros.add(mockOtherHero1);

        Node suggestedNode = opponentStrongerRPA.modifyPerception(nodes);
        assertThat(suggestedNode, nullValue());
    }

    private void prepareHeroesForScenarioV1() {
        when(mockYourHero.getLife()).thenReturn(ONE_LIFE);
        when(mockYourHero.getId()).thenReturn(HERO_ID_4);
        when(mockOtherHero1.getId()).thenReturn(HERO_ID_3);
        when(mockOtherHero1.getLife()).thenReturn(TWO_LIFE);
    }
}
