package com.bigfish.route;

import static com.bigfish.game.algo.RouteFinder.Direction.East;
import static com.bigfish.game.algo.RouteFinder.Direction.North;
import static com.bigfish.game.algo.RouteFinder.Direction.South;
import static com.bigfish.game.algo.RouteFinder.Direction.West;
import static com.bigfish.game.domain.NodeType.BEER;
import static com.bigfish.game.domain.NodeType.GO;
import static com.bigfish.game.domain.NodeType.PLAYER_1;
import static com.bigfish.game.domain.NodeType.PLAYER_2;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.bigfish.game.domain.Node;
import com.bigfish.game.domain.Position;

@RunWith(MockitoJUnitRunner.class)
public class NodeUTest {

    private static final String ID_1 = "ID1";
    private static final String ID_2 = "ID2";
    private static final String ID_3 = "ID3";
    private static final String ID_4 = "ID4";
    private static final String ID_5 = "ID5";
    private static final String ID_6 = "ID6";
    private static final long WEIGHT = 1;
    private static final Position POSITION_1 = new Position(1, 2);
    private static final Position POSITION_2 = new Position(2, 4);
    private static final Position WEST_POSITION = new Position(1, 1);
    private static final Position EAST_POSITION = new Position(1, 3);
    private static final Position NORTH_POSITION = new Position(0, 2);
    private static final Position SOUTH_POSITION = new Position(2, 2);

    private Node node1 = new Node(ID_1, PLAYER_1, POSITION_1);
    private Node node2 = new Node(ID_2, BEER, POSITION_2);

    private Node WEST_NEIGHBOUR = new Node(ID_3, BEER, WEST_POSITION);
    private Node EAST_NEIGHBOUR = new Node(ID_4, BEER, EAST_POSITION);
    private Node NORTH_NEIGHBOUR = new Node(ID_5, BEER, NORTH_POSITION);
    private Node SOUTH_NEIGHBOUR = new Node(ID_6, BEER, SOUTH_POSITION);

    @Mock
    private Node mockNode2;
    @Mock
    private Node mockNode3;
    @Mock
    private Node mockNode4;

    @Test
    public void equalsSanityCheck() {
        assertThat(node1, equalTo(node1));
    }

    @Test
    public void doesRouteGoThroughReturnsTrueIfNodeTypeFindsTypeInRoute() {
        prepareInitialRouteForGoThroughCheck();
        when(mockNode4.getNodeType()).thenReturn(PLAYER_2);
        boolean doesRouteGoThrough = node1.doesRouteGoThrough(PLAYER_2);
        assertThat(doesRouteGoThrough, equalTo(TRUE));
    }

    @Test
    public void doesRouteGoThroughReturnsFalseIfNodeTypeFindsTypeInRoute() {
        prepareInitialRouteForGoThroughCheck();
        when(mockNode4.getNodeType()).thenReturn(GO);
        boolean doesRouteGoThrough = node1.doesRouteGoThrough(PLAYER_2);
        assertThat(doesRouteGoThrough, equalTo(FALSE));
    }

    private void prepareInitialRouteForGoThroughCheck() {
        node1.setPrevNode(mockNode2);
        when(mockNode2.getPrevNode()).thenReturn(mockNode3);
        when(mockNode3.getPrevNode()).thenReturn(mockNode4);

        when(mockNode2.getNodeType()).thenReturn(GO);
        when(mockNode3.getNodeType()).thenReturn(GO);
    }

    @Test
    public void addNeighboursWorks() {
        node1.addNeighbour(node2, WEIGHT);
        assertThat(node1.getNeighbours().get(node2), equalTo(WEIGHT));
    }

    @Test
    public void isNeighbourReturnsTrueIfFindsSpecifiedType() {
        node1.addNeighbour(node2, WEIGHT);
        assertThat(node1.isNeighbour(BEER), equalTo(TRUE));
    }

    @Test
    public void isNeighbourReturnsFalseIfFindsSpecifiedType() {
        node1.addNeighbour(node1, WEIGHT);
        assertThat(node1.isNeighbour(BEER), equalTo(FALSE));
    }

    @Test
    public void getNeighbourOnTheWestWorks() {
        node1.addNeighbour(NORTH_NEIGHBOUR, WEIGHT);
        node1.addNeighbour(EAST_NEIGHBOUR, WEIGHT);
        node1.addNeighbour(SOUTH_NEIGHBOUR, WEIGHT);
        node1.addNeighbour(WEST_NEIGHBOUR, WEIGHT);
        assertThat(node1.getNeighbour(North).get(), is(NORTH_NEIGHBOUR));
        assertThat(node1.getNeighbour(East).get(), is(EAST_NEIGHBOUR));
        assertThat(node1.getNeighbour(South).get(), is(SOUTH_NEIGHBOUR));
        assertThat(node1.getNeighbour(West).get(), is(WEST_NEIGHBOUR));
    }
}
