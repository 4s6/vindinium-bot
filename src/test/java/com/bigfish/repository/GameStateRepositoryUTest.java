package com.bigfish.repository;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.bigfish.dto.GameState;

@RunWith(MockitoJUnitRunner.class)
public class GameStateRepositoryUTest {

    private final GameStateRepository gameStateRepository = new GameStateRepository();
    @Mock
    private GameState mockGameState;

    @Test
    public void getSetWorks() {
        gameStateRepository.set(mockGameState);
        assertThat(mockGameState, equalTo(gameStateRepository.get()));

    }
}
